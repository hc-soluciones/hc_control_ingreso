# Generated by Django 3.2.6 on 2021-09-07 20:37

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('control', '0002_encuesta_uuid'),
    ]

    operations = [
        migrations.AddField(
            model_name='encuesta',
            name='fecha_registro',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
