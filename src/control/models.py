from django.db import models
import uuid

class TipoPersona(models.Model):
    descripcion = models.CharField(max_length=30)

class Genero(models.Model):
    descripcion = models.CharField(max_length=30)

class Encuesta(models.Model):
    nombre = models.CharField(max_length=200)
    numero_control = models.CharField(max_length=20)
    email = models.CharField(max_length=30)
    telefono = models.CharField(max_length=20)
    tipo_persona = models.ForeignKey(TipoPersona, related_name='encuestas', on_delete=models.CASCADE, db_column='tipo_persona')
    url = models.CharField(max_length=200)
    # Preguntas
    contacto = models.BooleanField()
    temperatura_38 = models.BooleanField()
    tos_estornudos_frecuentes = models.BooleanField()
    malestar_dolor_cabeza = models.BooleanField()
    dificultad_respirar = models.BooleanField()
    uuid = models.UUIDField(
         default = uuid.uuid4,
         editable = False)
    fecha_registro = models.DateTimeField(auto_now_add=True)
    genero = models.ForeignKey(Genero, related_name='encuestas', on_delete=models.CASCADE, db_column='genero')