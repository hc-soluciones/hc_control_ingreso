$("#grdEncuestas").dxDataGrid({
    columnAutoWidth: true,
    height: 350,
    showBorders: true,
    filterRow:{
        visible:true
    },
    columns: [
        { dataField: 'nombre' },
        { dataField: 'numero_control' },
        { dataField: 'genero' },
        { dataField: 'email' },
        { dataField: 'telefono' },
        { dataField: 'tipo_persona' },
        { 
            dataField: 'contacto',
            caption: 'Tuvo contacto',
            falseText: 'No',
            trueText: 'Sí',
            dataType: 'boolean'
        },
        { 
            dataField: 'temperatura_38',
            caption: 'Temp > 38°',
            falseText: 'No',
            trueText: 'Sí',
            dataType: 'boolean'
        },
        { 
            dataField: 'tos_estornudos_frecuentes',
            falseText: 'No',
            trueText: 'Sí',
            dataType: 'boolean'
        },
        { 
            dataField: 'malestar_dolor_cabeza',
            falseText: 'No',
            trueText: 'Sí',
            dataType: 'boolean'
        },
        { 
            dataField: 'dificultad_respirar',
            falseText: 'No',
            trueText: 'Sí',
            dataType: 'boolean'
        },
        {
            dataField: 'fecha_registro',
            dataType: 'datetime',
            format:'dd/MM/yyyy hh:mm'
        },
    ],
    export: {
        enabled: true,
      },
      onExporting: function(e) {
        var workbook = new ExcelJS.Workbook();
        var worksheet = workbook.addWorksheet('Encuestas');
        
        DevExpress.excelExporter.exportDataGrid({
          component: e.component,
          worksheet: worksheet,
          autoFilterEnabled: true
        }).then(function() {
          workbook.xlsx.writeBuffer().then(function(buffer) {
            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'Encuestas.xlsx');
          });
        });
        e.cancel = true;
      },
})

function WithoutTime(dateTime) {
    var date = new Date(dateTime.getTime());
    date.setHours(0, 0, 0, 0);
    return date;
}
$("#dtFechaInicio").dxDateBox({
    type: "date",
    displayFormat: 'dd/MM/yyyy',
    value: WithoutTime(new Date())
});

$("#dtFechaFin").dxDateBox({
    type: "date",
    displayFormat: 'dd/MM/yyyy',
    value: new Date()
});


$("#btnBuscar").dxButton({
    stylingMode: "contained",
    text: "Buscar",
    type: "default",
    icon: 'search',
    width: 120,
    onClick: () => {
        let datapost = {
            fechaInicio: $('#dtFechaInicio').dxDateBox('instance').option('value').toJSON(),
            fechaFin: $('#dtFechaFin').dxDateBox('instance').option('value').toJSON(),
        }
        $.post('/get_encuestas/', datapost).done(
            (data) => {
                console.log(datapost)
                $("#grdEncuestas").dxDataGrid("instance").option("dataSource", data)
            }
        )
    }
});
