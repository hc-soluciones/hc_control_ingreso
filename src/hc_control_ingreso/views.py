from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from control.models import Encuesta, Genero, TipoPersona
import qrcode
import qrcode.image.svg
from io import BytesIO
import uuid, base64

from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required



def index(request):
	context = {'vacio': True}
	if request.method == "POST":
		context = {}
		nombre = request.POST.get('nombre')
		numero_control = request.POST.get('numero_control')
		genero = Genero.objects.get(pk=request.POST.get('genero'))
		email = request.POST.get('email')
		telefono = request.POST.get('telefono')
		tipo_persona = TipoPersona.objects.get(pk=request.POST.get('tipo_persona'))
		contacto = request.POST.get('contacto')
		temperatura_38 = request.POST.get('temperatura_38')
		tos_estornudos_frecuentes = request.POST.get('tos_estornudos_frecuentes')
		malestar_dolor_cabeza = request.POST.get('malestar_dolor_cabeza')
		dificultad_respirar = request.POST.get('dificultad_respirar')

		encuesta = Encuesta(
			nombre = nombre,
			numero_control = numero_control,
			genero = genero,
			email = email,
			telefono = telefono,
			tipo_persona = tipo_persona,
			url = '-',
			contacto = contacto,
			temperatura_38 = temperatura_38,
			tos_estornudos_frecuentes = tos_estornudos_frecuentes,
			malestar_dolor_cabeza = malestar_dolor_cabeza,
			dificultad_respirar = dificultad_respirar
		)

		encuesta.save()
		myuuid = encuesta.uuid
		dominio = 'http://127.0.0.1:8000/'
		url = f'{dominio}encuesta/{myuuid}'
		encuesta.url = url
		encuesta.save()

		factory = qrcode.image.svg.SvgImage
		qr = qrcode.QRCode(
			version=1,
			error_correction=qrcode.constants.ERROR_CORRECT_L,
			box_size=7,
			border=3,
			# back_color=(255, 195, 235),
			# fill_color=(55, 95, 35)
		)
		qr.add_data(str(encuesta.url))
		qr.make(fit=True)
		# img = qrcode.make(
		#     str(myuuid),
		#     box_size=10,
		# )
		img = qr.make_image(
			back_color='darkcyan',
			fill_color='white'
		)
		stream = BytesIO()
		img.save(stream)
		context["qr"] = base64.b64encode(stream.getvalue()).decode('ascii')
		context['uuid'] = encuesta.uuid
		context['vigencia'] = encuesta.fecha_registro

	return render(request, "index.html", context=context)

def num_control(request):
	context = {'vacio': True}
	if request.method == "POST":
		context = {}
		numero_control = request.POST.get('numero_control')
		if Encuesta.objects.filter(numero_control=numero_control).exists():
			encuesta = Encuesta.objects.filter(numero_control=numero_control).last()
			nombre = encuesta.nombre
			genero = encuesta.genero
			email = encuesta.email
			telefono = encuesta.telefono
			tipo_persona = encuesta.tipo_persona
			contacto = encuesta.contacto
			temperatura_38 = encuesta.temperatura_38
			tos_estornudos_frecuentes = encuesta.tos_estornudos_frecuentes
			malestar_dolor_cabeza = encuesta.malestar_dolor_cabeza
			dificultad_respirar = encuesta.dificultad_respirar
		else:
			nombre = ''
			genero = Genero.objects.get(pk=3)
			email = ''
			telefono = ''
			tipo_persona = TipoPersona.objects.get(pk=5)
			contacto = 0
			temperatura_38 = 0
			tos_estornudos_frecuentes = 0
			malestar_dolor_cabeza = 0
			dificultad_respirar = 0

		encuesta = Encuesta(
			nombre = nombre,
			numero_control = numero_control,
			genero = genero,
			email = email,
			telefono = telefono,
			tipo_persona = tipo_persona,
			url = '-',
			contacto = contacto,
			temperatura_38 = temperatura_38,
			tos_estornudos_frecuentes = tos_estornudos_frecuentes,
			malestar_dolor_cabeza = malestar_dolor_cabeza,
			dificultad_respirar = dificultad_respirar
		)

		encuesta.save()
		myuuid = encuesta.uuid
		dominio = 'http://127.0.0.1:8000/'
		url = f'{dominio}encuesta/{myuuid}'
		encuesta.url = url
		encuesta.save()

		factory = qrcode.image.svg.SvgImage
		qr = qrcode.QRCode(
			version=1,
			error_correction=qrcode.constants.ERROR_CORRECT_L,
			box_size=7,
			border=3,
			# back_color=(255, 195, 235),
			# fill_color=(55, 95, 35)
		)
		qr.add_data(str(encuesta.url))
		qr.make(fit=True)
		# img = qrcode.make(
		#     str(myuuid),
		#     box_size=10,
		# )
		img = qr.make_image(
			back_color='darkcyan',
			fill_color='white'
		)
		stream = BytesIO()
		img.save(stream)
		context["qr"] = base64.b64encode(stream.getvalue()).decode('ascii')
		context['uuid'] = encuesta.uuid
		context['vigencia'] = encuesta.fecha_registro

	return render(request, "num_control.html", context=context)

@login_required
def reporte(request):
	return render(request,'reporte.html',{})

@login_required
def home(request):
	return render(request,'base.html',{})

def login(request):
	if 'next' in request.GET:
		next = request.GET['next']
	else:
		next = '/'

	context = {
		'error': False,
		'next': next
	}

	if not request.user.is_anonymous:
		return HttpResponseRedirect(reverse('home'))

	if request.method == 'POST':
		formulario_acceso = AuthenticationForm(request.POST)
		if formulario_acceso.is_valid:			
			username = request.POST.get('username')
			password = request.POST.get('password')
			account = authenticate(username=username, password=password)
			if account is not None:
				if account.is_active:
					auth_login(request, account)
					return HttpResponseRedirect(next)
				else:
					context = {
						'status': 'No autorizado',
						'message': 'Esta cuenta ha sido deshabilitada.',
						'error': True,
						'next': next
					}
			else:
				context = {
					'status': 'No autorizado',
					'message': 'Combinación inválida.',
					'error': True,
					'next': next
				}
		else:
			pass
	return render(request, 'login.html', context)

@login_required()
def logout(request):
	auth_logout(request)
	return HttpResponseRedirect(reverse('login'))

@login_required
def get_encuestas(request):
	data = []
	fechaInicio = request.POST.get('fechaInicio')
	fechaFin = request.POST.get('fechaFin')
	encuestas = Encuesta.objects.filter(fecha_registro__gte=fechaInicio, fecha_registro__lte=fechaFin)
	for encuesta in encuestas:
		tmp = {
			'nombre': encuesta.nombre,
			'numero_control': encuesta.numero_control,
			'genero': encuesta.genero.descripcion,
			'email': encuesta.email,
			'telefono': encuesta.telefono,
			'tipo_persona': encuesta.tipo_persona.descripcion,
			'contacto': encuesta.contacto,
			'temperatura_38': encuesta.temperatura_38,
			'tos_estornudos_frecuentes': encuesta.tos_estornudos_frecuentes,
			'malestar_dolor_cabeza': encuesta.malestar_dolor_cabeza,
			'dificultad_respirar': encuesta.dificultad_respirar,
			'fecha_registro': encuesta.fecha_registro	,
		}
		data.append(tmp)

	return JsonResponse(data, safe=False)