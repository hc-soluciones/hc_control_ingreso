"""hc_control_ingreso URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import *
from django.conf import settings
from django.conf.urls.static import static
from control.models import *

admin.site.register(Genero)
admin.site.register(TipoPersona)
admin.site.site_header = "Control de Ingresos Admin"
admin.site.site_title = "Panel de Control"
admin.site.index_title = "Bienvenido al Panel de Administración"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('encuesta', index),
    path('encuesta_num_control', num_control),
    path('', home),
    path('reporte/', reporte),
    path('login/', login, name='login'),
    path('logout/', logout),
    path('get_encuestas/', get_encuestas),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
